
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-11 20:04:06
 */
import moment from 'moment'
import { getBillCode } from '@/api/finance/comego-pay-bill.js'

export default {
  data() {
    return {}
  },
  methods: {
    initSaveData() {
      const date = moment().format('YYYY-MM-DD HH:mm:ss')
      //
      this.$set(this.dataObj, 'businessTime', date)
      this.$set(this.dataObj, 'handUserId', this.currentUserId)
      this.getBillCode()
    },
    getBillCode() {
      getBillCode().then(res => {
        if (!res.data.flag) this.$message.error('获取票据编号失败！')
        const code = res.data.data
        this.$set(this.dataObj, 'billNo', code)
      })
    }
  }
}

