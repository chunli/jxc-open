/*
 * @Descripttion: 表格列数据
 * @version:
 * @Author: cxguo
 * @Date: 2019-10-18 15:37:24
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 15:05:24
 */
// import { getTableIndexColHeader } from '@/utils/vxe-table/render.js'

export default {
  data() {
    return {
      currentRowIndex: null,
      taxColumnsData: [
        { field: 'taxRate',
          title: '税率(%)',
          showOverflow: true,
          width: 100,
          editRender: { name: 'CxInputNumber', props: { size: 'mini' }}
        },
        { field: 'taxAmount',
          title: '税额',
          showOverflow: true,
          width: 100
        }
      ],
      originColumnsData: [
        { title: '序号',
          type: 'seq',
          width: 80,
          field: 'oper',
          align: 'center',
          slots: {
            default: ({ row, seq }) => {
              const rowId = row._XID
              const value = seq
              if (this.currentRowIndex !== rowId) return [<span>{value}</span>]
              const addData = {
                props: {
                  circle: true,
                  type: 'success',
                  size: 'mini',
                  icon: 'el-icon-plus'
                },
                on: {
                  'click': () => {
                    this.btnAddTableData()
                  }
                }
              }
              const delData = {
                props: {
                  circle: true,
                  type: 'danger',
                  size: 'mini',
                  icon: 'el-icon-close'
                },
                on: {
                  'click': () => {
                    this.btnRemoveTableData(row)
                  }
                }
              }
              return [
                <div class='btn-group table-oper'>
                  <el-button {...addData}/>
                  <el-button {...delData}/>
                </div>
              ]
            }
          }
        },
        { field: 'name',
          title: '商品名称/规格',
          positionDisable: true,
          showOverflow: true,
          width: 230,
          editRender: { name: 'CxSelectGoods', props: { size: 'mini' },
            events: { click: this.btnChooseGoods, onSelectData: this.onSelectData }
          }
        },
        { field: 'code', title: '商品编号', width: 140 },
        { field: 'quantity', title: '数量', width: 100,
          editRender: { name: 'CxInputNumber', props: { size: 'mini', min: 0 }}
        },
        { field: 'unitName', title: '单位', width: 120,
          editRender: { name: 'CxSelectUnit',
            events: { change: this.onUnitChange }
          }
        },
        { field: 'price', title: '单价', width: 100,
          editRender: { name: 'CxSelectPrice',
            events: {}
          }
        },
        { field: 'total', title: '金额', width: 100, align: 'right' },
        { field: 'remark', title: '备注', width: 120,
          editRender: { name: 'ElInput', props: { size: 'mini' }}
        }
      ]
    }
  },
  methods: {
    onUnitChange({ unitData, row }) {
      const { id, name, multi } = unitData
      this.$set(row, 'unitId', id)
      this.$set(row, 'unitName', name)
      this.$set(row, 'unitMulti', multi)
    }
  }
}
