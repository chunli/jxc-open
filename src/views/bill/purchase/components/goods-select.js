/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-16 22:20:19
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-09 08:33:02
 */
import GoodsSelect from '@/views/goods-select'

export default {
  extends: GoodsSelect,
  mixins: [GoodsSelect],
  props: {
  },
  data() {
    return {
    }
  },
  methods: {
  }
}
