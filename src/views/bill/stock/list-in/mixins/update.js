
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-07 08:45:12
 */
import { getWaiteBillDetail } from '@/api/bill/bill-stock-in.js'

export default {
  data() {
    return {}
  },
  methods: {
    initUpdateData() {
      const billId = this.billId
      return this.getWaiteBillDetail(billId).then(data => {
        const { bill, billDetailList } = data
        this.setBillData({ bill, billDetailList })
      })
    },
    initTransData() {
      const billId = this.billId
      return this.getWaiteBillDetail(billId).then(data => {
        const { bill, billDetailList } = data
        billDetailList.forEach(item => {
          const { quantity, changeQuantity } = item
          const taegetQty = this.$amount(quantity).subtract(changeQuantity).format()
          item.quantity = taegetQty
          item.maxq = taegetQty
        })
        this.setBillData({ bill, billDetailList })
      })
    },
    setBillData({ bill, billDetailList }) {
      this.dataObj = bill
      this.$refs.GoodsSelect.setTableData(billDetailList)
    },
    setDataObj(dataObj) {
      Object.keys(dataObj).forEach(key => {
        this.$set(this.dataObj, key, dataObj[key])
      })
    },
    getWaiteBillDetail(billId) {
      return new Promise((resolve, reject) => {
        const billCat = this.billCat
        const params = { billId, billCat }
        getWaiteBillDetail(params).then(res => {
          if (!res.data.flag) {
            return reject(new Error('获取采购单商品失败！'))
          }
          const data = res.data.data
          resolve(data)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
}

