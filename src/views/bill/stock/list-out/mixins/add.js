
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-23 20:38:09
 */
import moment from 'moment'
import { getBillcode } from '@/api/bill/bill-stock-out.js'
export default {
  data() {
    return {}
  },
  methods: {
    initSaveData() {
      this.dataObj = {}
      this.getBillCode()
      const date = moment().format('YYYY-MM-DD HH:mm:ss')
      this.$set(this.dataObj, 'businessTime', date)
      this.$set(this.dataObj, 'amountOther', '0.00')
    },
    getBillCode() {
      getBillcode().then(res => {
        if (!res.data.flag) this.$message.error('获取票据编号失败！')
        const code = res.data.data
        this.$set(this.dataObj, 'billNo', code)
      })
    }
  }
}

