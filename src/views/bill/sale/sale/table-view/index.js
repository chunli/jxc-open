/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-16 22:20:19
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-15 18:56:35
 */
import GoodsSelect from '@/views/goods-select'

export default {
  extends: GoodsSelect,
  mixins: [GoodsSelect],
  props: {
  },
  data() {
    return {}
  },
  created() {
    const originColumnsData = this.originColumnsData
    const nameCol = originColumnsData.find(item => { return item.field === 'name' })
    const priceCol = originColumnsData.find(item => { return item.field === 'price' })
    const quantityCol = originColumnsData.find(item => { return item.field === 'quantity' })
    const unitNameCol = originColumnsData.find(item => { return item.field === 'unitName' })
    this.$delete(nameCol, 'editRender')
    this.$delete(quantityCol, 'editRender')
    this.$delete(priceCol, 'editRender')
    this.$delete(unitNameCol, 'editRender')
    // 处理价格
    this.handlePriceCol(priceCol, originColumnsData)
    this.originColumnsData = originColumnsData
  },
  watch: {
  },
  methods: {
    handlePriceCol(priceCol, originColumnsData) {
      priceCol.title = '折后单价'
      const discountCols = [
        { field: 'priceDef', title: '单价', width: 100 },
        { field: 'discount', title: '折扣(%)', width: 100 },
        priceCol
      ]
      const priceColIndex = originColumnsData.findIndex(item => { return item.field === 'price' })
      originColumnsData.splice(priceColIndex, 1, ...discountCols)
    }
  }
}
