
/*
 * @Descripttion: 商品分类接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 15:01:47
 */
import axios from '@/utils/request'
const baseUrl = '/supplierCat'

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listAllData`,
    method: 'post',
    data: params
  })
}

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/saveData`,
    method: 'post',
    data: params
  })
}

export function delData(id) {
  return axios.request({
    url: `${baseUrl}/delete/${id}`,
    method: 'post',
    data: id
  })
}
