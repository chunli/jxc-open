/*
 * @Descripttion: 财务往来账
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-03-17 15:12:31
 */
import axios from '@/utils/request'
const baseUrl = '/fin_paydaily'

export function addDataIn(params) {
  return axios.request({
    url: `${baseUrl}/add_dayily_in`,
    method: 'post',
    data: params
  })
}

export function addDataOut(params) {
  return axios.request({
    url: `${baseUrl}/add_dayily_out`,
    method: 'post',
    data: params
  })
}

export function listDataOut(params) {
  return axios.request({
    url: `${baseUrl}/list_dayily_out`,
    method: 'post',
    data: params
  })
}

export function listDataIn(params) {
  return axios.request({
    url: `${baseUrl}/list_dayily_in`,
    method: 'post',
    data: params
  })
}
