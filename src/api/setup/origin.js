/*
 * @Descripttion: 期初和结账
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-07 14:01:58
 */
import axios from '@/utils/request'
const baseUrl = '/origin'

export function balance(params) {
  return axios.request({
    url: `${baseUrl}/balance`,
    method: 'post',
    data: params
  })
}

export function unbalance(params) {
  return axios.request({
    url: `${baseUrl}/unbalance`,
    method: 'post',
    data: params
  })
}

export function getBalanceTime(params) {
  return axios.request({
    url: `${baseUrl}/getBalanceTime`,
    method: 'post',
    data: params
  })
}

export function listOrigin(params) {
  return axios.request({
    url: `${baseUrl}/listOriginPage`,
    method: 'post',
    data: params
  })
}

export function listStock(params) {
  return axios.request({
    url: `${baseUrl}/listStockPage`,
    method: 'post',
    data: params
  })
}

export function listDeptRece(params) {
  return axios.request({
    url: `${baseUrl}/listDeptRecePage`,
    method: 'post',
    data: params
  })
}

export function listDeptPay(params) {
  return axios.request({
    url: `${baseUrl}/listDeptPayPage`,
    method: 'post',
    data: params
  })
}

export function listAccountOrigin(params) {
  return axios.request({
    url: `${baseUrl}/listAccountOriginPage`,
    method: 'post',
    data: params
  })
}
