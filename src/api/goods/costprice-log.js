/*
 * @Descripttion: 商品分类接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-07 15:39:04
 */
import axios from '@/utils/request'
const baseUrl = '/log_cost_price'

export function listDataBySku(params) {
  return axios.request({
    url: `${baseUrl}/list_data_by_skuid`,
    method: 'post',
    data: params
  })
}
