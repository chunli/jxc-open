/*
 * @Descripttion: 商品接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 16:18:31
 */
import axios from '@/utils/request'
const baseUrl = '/goodSku'

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/saveData`,
    method: 'post',
    data: params
  })
}

export function deleteData(skuId) {
  return axios.request({
    url: `${baseUrl}/delete/${skuId}`,
    method: 'post',
    data: skuId
  })
}

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

export function selectContainerStock(params) {
  return axios.request({
    url: `${baseUrl}/listDataAndStockPage`,
    method: 'post',
    data: params
  })
}

export function listDataMore(params) {
  return axios.request({
    url: `${baseUrl}/listDataMorePage`,
    method: 'post',
    data: params
  })
}

export function listHotSaleData(params) {
  return axios.request({
    url: `${baseUrl}/listHotSaleDataPage`,
    method: 'post',
    data: params
  })
}

export function listMinStock() {
  return axios.request({
    url: `${baseUrl}/listMinStockPage`,
    method: 'post',
    data: {}
  })
}

export function listMaxStock() {
  return axios.request({
    url: `${baseUrl}/listMaxStockPage`,
    method: 'post',
    data: {}
  })
}

export function getPriceBySkuAndUnit(params) {
  const { skuId, unitId } = params
  return axios.request({
    url: `${baseUrl}/getPrice/${skuId}/${unitId}`,
    method: 'post',
    data: params
  })
}

export function getSkuDetailById(skuId) {
  return axios.request({
    url: `${baseUrl}/getDetailData/${skuId}`,
    method: 'post',
    data: skuId
  })
}

export function getSkuByBarcode(barcode) {
  return axios.request({
    url: `${baseUrl}/getData/${barcode}`,
    method: 'post',
    data: barcode
  })
}

export function getGoodSkuCode(params) {
  return axios.request({
    url: `${baseUrl}/getSkuNo`,
    method: 'post',
    data: params
  })
}

export function getUnitlistBySkuid(params) {
  return axios.request({
    url: `${baseUrl}/get_unitlist_by_skuid`,
    method: 'post',
    data: params
  })
}
export function countByCode(params) {
  return axios.request({
    url: `${baseUrl}/count_by_code`,
    method: 'post',
    data: params
  })
}

export function delUnit(params) {
  return axios.request({
    url: `${baseUrl}/del_unit`,
    method: 'post',
    data: params
  })
}

