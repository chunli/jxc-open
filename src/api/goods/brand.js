/*
 * @Descripttion: 商品品牌接口api
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2019-08-20 16:32:29
 */
import axios from '@/utils/request'

export function fetchList(params) {
  return axios.request({
    url: '/brand/list',
    method: 'get',
    params: params
  })
}
export function createBrand(data) {
  return axios.request({
    url: '/brand/create',
    method: 'post',
    data: data
  })
}
export function updateShowStatus(data) {
  return axios.request({
    url: '/brand/update/showStatus',
    method: 'post',
    data: data
  })
}

export function updateFactoryStatus(data) {
  return axios.request({
    url: '/brand/update/factoryStatus',
    method: 'post',
    data: data
  })
}

export function deleteBrand(id) {
  return axios.request({
    url: '/brand/delete/' + id,
    method: 'get'
  })
}

export function getBrand(id) {
  return axios.request({
    url: '/brand/' + id,
    method: 'get'
  })
}

export function updateBrand(id, data) {
  return axios.request({
    url: '/brand/update/' + id,
    method: 'post',
    data: data
  })
}

