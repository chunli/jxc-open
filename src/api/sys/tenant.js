/*
 * @Descripttion: 接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 21:27:58
 */
import axios from '@/utils/request'
const baseUrl = '/tenant'

export function register(params) {
  return axios.request({
    url: `${baseUrl}/register`,
    method: 'post',
    data: params
  })
}

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/updateData`,
    method: 'post',
    data: params
  })
}

export function getTenantInfo(params) {
  return axios.request({
    url: `${baseUrl}/getTenantInfo`,
    method: 'post',
    data: params
  })
}

export function getVerifyCode(params) {
  return axios.request({
    url: `${baseUrl}/getVerifyCode`,
    method: 'post',
    data: params
  })
}

export function clearAllData(params) {
  return axios.request({
    url: `${baseUrl}/resetData`,
    method: 'post',
    data: params
  })
}

