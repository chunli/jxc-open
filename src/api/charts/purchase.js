/*
 * @Descripttion: 销售图表
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 16:31:08
 */
import axios from '@/utils/request'
const baseUrl = '/charts/purchase'

export function listByGoodGroupbyDay(params) {
  return axios.request({
    url: `${baseUrl}/listby_good_groupby_day/${params.skuId}`,
    method: 'post',
    data: params
  })
}

export function listByGoodGroupbyMonth(params) {
  return axios.request({
    url: `${baseUrl}/listby_good_groupby_month/${params.skuId}`,
    method: 'post',
    data: params
  })
}

export function listByGoodGroupbyYear(params) {
  return axios.request({
    url: `${baseUrl}/listby_good_groupby_year/${params.skuId}`,
    method: 'post',
    data: params
  })
}

export function getBillRangeTimeBySku(skuId) {
  return axios.request({
    url: `${baseUrl}/get_bill_range_time_bysku/${skuId}`,
    method: 'post',
    data: {}
  })
}

export function listByComegoGroupbyDay(params) {
  return axios.request({
    url: `${baseUrl}/listby_comego_groupby_day/${params.comegoId}`,
    method: 'post',
    data: params
  })
}

export function listByComegoGroupbyMonth(params) {
  return axios.request({
    url: `${baseUrl}/listby_comego_groupby_month/${params.comegoId}`,
    method: 'post',
    data: params
  })
}

export function listByComegoGroupbyYear(params) {
  return axios.request({
    url: `${baseUrl}/listby_comego_groupby_year/${params.comegoId}`,
    method: 'post',
    data: params
  })
}

export function getBillRangeTimeByComego(comegoId) {
  return axios.request({
    url: `${baseUrl}/get_bill_range_time_bycomego/${comegoId}`,
    method: 'post',
    data: {}
  })
}

