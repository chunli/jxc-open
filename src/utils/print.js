/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-08-20 14:08:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-20 14:09:48
 */

/**
 * btnPrint 打印方法
 * removeFooter 去除页眉页脚
 */

export const Print = (printDom) => {
  // 1.设置要打印的区域 div的className
  var newstr = printDom.innerHTML
  // 2. 还原：将旧的页面储存起来，当打印完成后返给给页面。
  var oldstr = document.body.innerHTML
  // 3. 复制给body，并执行window.print打印功能
  const body = document.body
  body.style.overflow = 'visible'
  body.innerHTML = newstr
  if (!!window.ActiveXObject || 'ActiveXObject' in window) { // 是否ie
    removeFooter()
  }
  window.print()
  document.body.innerHTML = oldstr
  // 重新加载页面，以刷新数据
  window.location.reload()
}

const removeFooter = () => {
  var hkey_path
  hkey_path = 'HKEY_CURRENT_USER\\Software\\Microsoft\\Internet Explorer\\PageSetup\\'
  try {
    // eslint-disable-next-line no-undef
    var RegWsh = new ActiveXObject('WScript.Shell')
    RegWsh.RegWrite(hkey_path + 'header', '')
    RegWsh.RegWrite(hkey_path + 'footer', '')
  } catch (e) {
    console.error(e.message)
  }
  // print().then(res => {
  //   debugger
  // })
}

