/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-07-04 01:36:52
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-16 14:20:00
 */

export default function getPageTitle(pageTitle, title) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
