/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-07-04 01:36:52
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-16 14:25:32
 */

module.exports = {
  title: '不二掌柜进销存V1.0',

  /**
   * @type {boolean} true | false
   * @description Whether show the settings right-panel
   */
  showSettings: false,

  /**
   * @type {boolean} true | false
   * @description tab菜单
   */
  tagsView: true,

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true,

  /**
   * @type {string | array} 'production' | ['production', 'development']
   * @description Need show err logs component.
   * The default is only used in the production env
   * If you want to also use it in dev, you can pass ['production', 'development']
   */
  errorLog: 'production'
}
