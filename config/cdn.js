/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-08-20 14:47:26
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-28 07:56:46
 */
// cdn.js
module.exports = {
  // 以下是示例,需要引入什么,自己通过bundle分析判断
  'vue': {
    'name': 'Vue',
    'cdn': {
      'css': {
        'default': '',
        'min': ''
      },
      'js': {
        'default': 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js',
        'min': 'vue.min@2.6.10.js'
      }
    }
  },
  'moment': {
    'name': 'moment',
    'cdn': {
      'css': {
        'default': '',
        'min': ''
      },
      'js': {
        'default': 'moment.min@2.24.0.js',
        'min': 'moment.min@2.24.0.js'
      }
    }
  },
  'element-ui': {
    'name': 'ELEMENT',
    'cdn': {
      'css': {
        'default': '',
        'min': ''
      },
      'js': {
        'default': 'element-ui@2.11.1.js',
        'min': 'element-ui@2.11.1.js'
      }
    }
  }
}

